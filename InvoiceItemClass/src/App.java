public class App {
    public static void main(String[] args) throws Exception {
        InvoiceItem invoice1 = new InvoiceItem("A01", "Laptop", 1000, 1500);
        InvoiceItem invoice2 = new InvoiceItem("A02", "PC", 1200, 1000);

        System.out.println(invoice1.toString());
        System.out.println(invoice2.toString());

        //tổng giá
        System.out.println(invoice1.getTotal());
        System.out.println(invoice2.getTotal());
    }
}
